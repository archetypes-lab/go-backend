package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/golobby/container/v2"

	"gitlab.com/archetypes-lab/go-backend/internal/config"
	"gitlab.com/archetypes-lab/go-backend/pkg/context"
	"gitlab.com/archetypes-lab/go-backend/pkg/utils"
)

func main() {

	defer cleanup()

	config.ApplicationConfig()
	config.EventsConfig()
	config.RepositoryConfig()
	config.ServiceConfig()
	config.RestConfig()

	context.TriggerPostCronstruct()

	container.Call(func(ginEngine *gin.Engine, configProp utils.ConfigPropertiesUtils) {
		ginEngine.Run(fmt.Sprintf(":%s", configProp.GetStrProp("GIN_PORT")))
	})

}

func cleanup() {
	fmt.Println("CERRANDO TODO")
	context.TriggerPreDestroy()
}

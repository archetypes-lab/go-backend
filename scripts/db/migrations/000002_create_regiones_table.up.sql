BEGIN;

CREATE TABLE IF NOT EXISTS regiones(
   id_region int4 PRIMARY KEY,
   nombre VARCHAR (255) UNIQUE NOT NULL,
   numero int4 NOT NULL
);

INSERT INTO regiones (id_region, nombre, numero) values
(1, 'I Región ', 1),
(2, 'II Región ', 2),
(3, 'III Región ', 3),
(4, 'IV Región de Coquimbo', 4),
(5, 'V Región de Valparaíso', 5),
(6, 'VI Región ', 6),
(7, 'VII Región ', 7),
(8, 'VIII Región del Bio Bio', 8),
(9, 'IX Región ', 9),
(10, 'X Región ', 10),
(11, 'XI Región ', 11),
(12, 'XII Región ', 12),
(13, 'XIII Región Metropolitana ', 13),
(14, 'XIV Región ', 14),
(15, 'XV Región ', 15);

COMMIT;
package model

type Region struct {
	IdRegion int
	Nombre   string
	Numero   int
}

func (Region) TableName() string {
	return "regiones"
}

package model

type Persona struct {
	IdPersona int64
	Nombres   string
	Apellidos string
}

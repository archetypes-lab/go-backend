package service

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/archetypes-lab/go-backend/internal/events"
	"go.uber.org/zap"
)

type HelloWorldService struct {
	producer *events.HelloWorldQueueProducer
	consumer *events.HelloWorldQueueConsumer
	logger   *zap.Logger
}

func CreateHelloWorldService(
	producer *events.HelloWorldQueueProducer,
	consumer *events.HelloWorldQueueConsumer,
	logger *zap.Logger,
) *HelloWorldService {
	return &HelloWorldService{
		producer: producer,
		consumer: consumer,
		logger:   logger,
	}
}

func (service *HelloWorldService) PostConstruct() {
	threads := 3
	for i := 0; i < threads; i++ {
		go func() {
			for msg := range service.consumer.GetOutput() {
				msgBody := string(msg.Body)
				service.logger.Info(fmt.Sprintf("Llego evento Hello World: %s", msgBody))
				time.Sleep(15 * time.Second)
				aleatorio := rand.Intn(100)
				service.logger.Info(fmt.Sprintf("Terminado evento Hello World: %s, aleatorio: %d", msgBody, aleatorio))
				if aleatorio > 50 {
					msg.Ack(false)
				} else {
					msg.Nack(false, true)
				}
			}
		}()
	}
}

func (service *HelloWorldService) SendHelloWorld(msg string) error {
	return service.producer.Send(msg)
}

package service

import (
	"gitlab.com/archetypes-lab/go-backend/internal/model"
	repo "gitlab.com/archetypes-lab/go-backend/internal/repository"
)

type RegionService struct {
	repository *repo.RegionRepository
}

func CreateRegionService(repository *repo.RegionRepository) *RegionService {
	service := &RegionService{repository: repository}
	return service
}

func (service *RegionService) FindAll() []model.Region {
	return service.repository.FindAll()
}

func (service *RegionService) FindById(idRegion int) model.Region {
	return service.repository.FindById(idRegion)
}

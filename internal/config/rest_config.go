package config

import (
	"github.com/gin-gonic/gin"
	"github.com/golobby/container/v2"
	"gitlab.com/archetypes-lab/go-backend/internal/rest"
	"gitlab.com/archetypes-lab/go-backend/internal/service"
	"gitlab.com/archetypes-lab/go-backend/pkg/context"
	"go.uber.org/zap"
)

func RestConfig() {

	// PING REST
	container.Singleton(func(
		ginEngine *gin.Engine,
		logger *zap.Logger,
		helloWorldService *service.HelloWorldService,
	) *rest.PingRest {
		pingRest := rest.CreatePingRest(logger, ginEngine, helloWorldService)
		context.AddListenPostConstruct(pingRest)
		return pingRest
	})

	// REGION REST
	container.Singleton(func(ginEngine *gin.Engine, regionService *service.RegionService) *rest.RegionRest {
		regionRest := rest.CreateRegionRest(ginEngine, regionService)
		context.AddListenPostConstruct(regionRest)
		return regionRest
	})

}

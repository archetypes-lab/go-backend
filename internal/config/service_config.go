package config

import (
	"github.com/golobby/container/v2"
	"gitlab.com/archetypes-lab/go-backend/internal/events"
	"gitlab.com/archetypes-lab/go-backend/internal/repository"
	"gitlab.com/archetypes-lab/go-backend/internal/service"
	"gitlab.com/archetypes-lab/go-backend/pkg/context"

	"go.uber.org/zap"
)

func ServiceConfig() {

	// HELLO WORLD SERVICE
	container.Singleton(func(
		producer *events.HelloWorldQueueProducer,
		consumer *events.HelloWorldQueueConsumer,
		logger *zap.Logger,
	) *service.HelloWorldService {
		service := service.CreateHelloWorldService(producer, consumer, logger)
		context.AddListenPostConstruct(service)
		return service
	})

	// REGION SERVICE
	container.Singleton(func(regionRepository *repository.RegionRepository) *service.RegionService {
		return service.CreateRegionService(regionRepository)
	})

}

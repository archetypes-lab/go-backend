package config

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/golobby/container/v2"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/archetypes-lab/go-backend/pkg/context"
	"gitlab.com/archetypes-lab/go-backend/pkg/utils"
	"gitlab.com/archetypes-lab/go-backend/pkg/utils/db"
	"go.uber.org/zap"

	"github.com/streadway/amqp"
)

func ApplicationConfig() {

	// CONFIG PROPERTIES UTILS
	container.Singleton(func() utils.ConfigPropertiesUtils {
		return utils.CreateConfigStoreUtils()
	})

	// GIN
	container.Singleton(func() *gin.Engine {
		return gin.Default()
	})

	// LOGGER
	container.Singleton(func() *zap.Logger {
		logger, _ := zap.NewProduction()
		return logger
	})

	// DATABASE MIGRATION
	container.Singleton(func(logger *zap.Logger, configProp utils.ConfigPropertiesUtils) *db.PostgresMigrationUtils {
		mc := db.CreatePostgresMigrationUtils(
			configProp.GetStrProp("DATABASE_USER"),
			configProp.GetStrProp("DATABASE_PASS"),
			configProp.GetStrProp("DATABASE_HOST"),
			int(configProp.GetIntProp("DATABASE_PORT")),
			configProp.GetStrProp("DATABASE_DBNAME"),
			configProp.GetStrProp("DATABASE_SSLMODE"),
			logger)
		context.AddListenPostConstruct(mc)
		return mc
	})

	// GORM POSTGRES DB

	container.Singleton(func(configProp utils.ConfigPropertiesUtils) *gorm.DB {

		dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=%s TimeZone=%s",
			configProp.GetStrProp("DATABASE_HOST"),
			configProp.GetStrProp("DATABASE_USER"),
			configProp.GetStrProp("DATABASE_PASS"),
			configProp.GetStrProp("DATABASE_DBNAME"),
			int(configProp.GetIntProp("DATABASE_PORT")),
			configProp.GetStrProp("DATABASE_SSLMODE"),
			configProp.GetStrProp("DATABASE_TIMEZONE"))

		gormConfig := gorm.Config{}

		db, err := gorm.Open(postgres.Open(dsn), &gormConfig)
		if err != nil {
			panic("failed to connect to postgres database")
		}

		return db
	})

	// AMQP Configuration

	container.Singleton(func(logger *zap.Logger, configProp utils.ConfigPropertiesUtils) *amqp.Connection {
		amqpConnString := fmt.Sprintf("amqp://%s:%s@%s:%d/%s",
			configProp.GetStrProp("AMQP_USER"),
			configProp.GetStrProp("AMQP_PASSWORD"),
			configProp.GetStrProp("AMQP_HOST"),
			configProp.GetIntProp("AMQP_PORT"),
			configProp.GetStrProp("AMQP_VHOST"))

		amqpConn, err := amqp.Dial(amqpConnString)
		if err != nil {
			panic("failed to connect to amqp server")
		}

		logger.Info("AMQP connection created succesfully!")

		context.AddListenPreDestroy(context.DoPreDestroyWith(func() {
			errClose := amqpConn.Close()
			if errClose != nil {
				logger.Info("Can not close AMQP connection")
			} else {
				logger.Info("AMQP connection closed successfully!")
			}
		}))

		return amqpConn
	})

	container.Singleton(func(amqpConn *amqp.Connection) *amqp.Channel {
		channel, err := amqpConn.Channel()
		if err != nil {
			panic("Can't create the amqp channel")
		}
		return channel
	})

}

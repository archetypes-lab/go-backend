package config

import (
	"github.com/golobby/container/v2"
	"github.com/streadway/amqp"
	"gitlab.com/archetypes-lab/go-backend/internal/events"
	"gitlab.com/archetypes-lab/go-backend/pkg/context"
	"gitlab.com/archetypes-lab/go-backend/pkg/utils"
	"go.uber.org/zap"
)

func EventsConfig() {

	// HELLO WORLD QUEUE PRODUCER

	container.Singleton(func(
		amqpChannel *amqp.Channel,
		configProp utils.ConfigPropertiesUtils,
	) *events.HelloWorldQueueProducer {
		return events.CreateHelloWorldQueueProducer(
			configProp.GetStrProp("HELLO_WORLD_QUEUE"),
			amqpChannel)
	})

	// HELO WORLD QUEUE CONSUMER

	container.Singleton(func(
		amqpChannel *amqp.Channel,
		configProp utils.ConfigPropertiesUtils,
		logger *zap.Logger,

	) *events.HelloWorldQueueConsumer {
		consumer := events.CreateHelloWorldQueueConsumer(
			configProp.GetStrProp("HELLO_WORLD_QUEUE"),
			amqpChannel,
			logger,
		)
		context.AddListenPostConstruct(consumer)
		return consumer
	})

}

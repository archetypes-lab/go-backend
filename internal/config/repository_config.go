package config

import (
	"github.com/golobby/container/v2"
	"gitlab.com/archetypes-lab/go-backend/internal/repository"
	"gorm.io/gorm"
)

func RepositoryConfig() {

	// REGION REPOSITORY

	container.Singleton(func(db *gorm.DB) *repository.RegionRepository {
		return repository.CreateRegionRepository(db)
	})

}

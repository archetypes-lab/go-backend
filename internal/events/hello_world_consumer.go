package events

import (
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type HelloWorldQueueConsumer struct {
	amqpChannel *amqp.Channel
	logger      *zap.Logger
	queue       amqp.Queue
	outputChan  chan amqp.Delivery
}

func CreateHelloWorldQueueConsumer(queueName string, amqpChannel *amqp.Channel, logger *zap.Logger) *HelloWorldQueueConsumer {
	queue, err := amqpChannel.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		nil)
	if err != nil {
		panic("Can't create hello world queue")
	}

	outputChan := make(chan amqp.Delivery)

	return &HelloWorldQueueConsumer{
		amqpChannel: amqpChannel,
		logger:      logger,
		queue:       queue,
		outputChan:  outputChan}
}

func (consumer *HelloWorldQueueConsumer) PostConstruct() {
	msgs, err := consumer.amqpChannel.Consume(
		consumer.queue.Name, // queue
		"",                  // consumer
		false,               // auto-ack
		false,               // exclusive
		false,               // no-local
		false,               // no-wait
		nil,                 // args
	)
	if err != nil {
		panic("Failed to register hello world queue consumer")
	}

	go func() {
		for msg := range msgs {
			consumer.outputChan <- msg
		}
	}()
}

func (consumer *HelloWorldQueueConsumer) GetOutput() <-chan amqp.Delivery {
	return consumer.outputChan
}

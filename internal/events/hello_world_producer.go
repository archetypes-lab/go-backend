package events

import (
	"github.com/streadway/amqp"
)

type HelloWorldQueueProducer struct {
	amqpChannel *amqp.Channel
	queue       amqp.Queue
}

func CreateHelloWorldQueueProducer(queueName string, amqpChannel *amqp.Channel) *HelloWorldQueueProducer {
	queue, err := amqpChannel.QueueDeclare(
		queueName, // queue
		false,     // durable
		false,     //delete when unused
		false,     //exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		panic("Can't create hello world queue")
	}
	return &HelloWorldQueueProducer{amqpChannel: amqpChannel, queue: queue}
}

func (producer *HelloWorldQueueProducer) Send(msg string) error {
	err := producer.amqpChannel.Publish(
		"",                  // exchange
		producer.queue.Name, // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(msg),
		})
	return err
}

package repository

import (
	"gitlab.com/archetypes-lab/go-backend/internal/model"
	"gorm.io/gorm"
)

type RegionRepository struct {
	db *gorm.DB
}

func CreateRegionRepository(db *gorm.DB) *RegionRepository {
	return &RegionRepository{db: db}
}

func (repo *RegionRepository) FindAll() []model.Region {
	var regiones []model.Region
	repo.db.Find(&regiones)
	return regiones
}

func (repo *RegionRepository) FindById(idRegion int) model.Region {
	var region model.Region
	repo.db.First(&region, idRegion)
	return region
}

package rest

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/archetypes-lab/go-backend/internal/service"
)

type RegionRest struct {
	ginEngine *gin.Engine
	service   *service.RegionService
}

func CreateRegionRest(ginEngine *gin.Engine, service *service.RegionService) *RegionRest {
	regionRest := &RegionRest{ginEngine: ginEngine, service: service}
	return regionRest
}

func (rest *RegionRest) PostConstruct() {
	r := rest.ginEngine

	r.GET("/regiones/:idRegion", func(c *gin.Context) {
		idRegionParam := c.Param("idRegion")
		idRegion, _ := strconv.ParseInt(idRegionParam, 10, 32)
		region := rest.service.FindById(int(idRegion))
		c.JSON(200, region)
	})

	r.GET("/regiones/", func(c *gin.Context) {
		regiones := rest.service.FindAll()
		c.JSON(200, regiones)
	})

}

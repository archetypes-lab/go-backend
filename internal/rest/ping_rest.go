package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/archetypes-lab/go-backend/internal/service"
	"go.uber.org/zap"
)

type PingRest struct {
	logger            *zap.Logger
	ginEngine         *gin.Engine
	helloWorldService *service.HelloWorldService
}

func CreatePingRest(
	logger *zap.Logger,
	ginEngine *gin.Engine,
	helloWorldService *service.HelloWorldService,
) *PingRest {
	return &PingRest{
		logger:            logger,
		ginEngine:         ginEngine,
		helloWorldService: helloWorldService,
	}
}

func (rest *PingRest) PostConstruct() {

	r := rest.ginEngine
	logger := rest.logger

	logger.Debug("=> Iniciando PingRest!")

	r.GET("/ping", func(c *gin.Context) {
		logger.Info("successfully performed http request")
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/hello", func(c *gin.Context) {
		msg := c.Query("msg")
		if msg == "" {
			c.JSON(400, gin.H{
				"error": "msg param not found",
			})
			return
		}
		err := rest.helloWorldService.SendHelloWorld(msg)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
		} else {
			c.JSON(200, gin.H{
				"msg": "ok",
			})
		}

	})

}

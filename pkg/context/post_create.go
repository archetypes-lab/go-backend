package context

type IPostConstrut interface {
	PostConstruct()
}

var postCronstructSlide []IPostConstrut = []IPostConstrut{}

func AddListenPostConstruct(postConstruct IPostConstrut) {
	postCronstructSlide = append(postCronstructSlide, postConstruct)
}

func TriggerPostCronstruct() {
	for _, element := range postCronstructSlide {
		element.PostConstruct()
	}
}

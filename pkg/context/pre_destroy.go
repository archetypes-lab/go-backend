package context

type IPreDestroy interface {
	PreDestroy()
}

var preDestroySlide []IPreDestroy = []IPreDestroy{}

func AddListenPreDestroy(preDestroy IPreDestroy) {
	preDestroySlide = append(preDestroySlide, preDestroy)
}

func TriggerPreDestroy() {
	for _, element := range preDestroySlide {
		element.PreDestroy()
	}
}

// anonymous implementation function option

type DoPreDestroyWith func()

func (preDetroy DoPreDestroyWith) PreDestroy() {
	preDetroy()
}

package utils

import (
	"fmt"
	"os"
	"strings"

	"github.com/ovh/configstore"
)

type ConfigStoreUtils struct {
	items *configstore.ItemList
}

const configFilePathTemplate string = "configs/application%PROFILE%.yml"
const activeProfileEnvName string = "ACTIVE_PROFILE"

func CreateConfigStoreUtils() *ConfigStoreUtils {
	activeProfile := os.Getenv(activeProfileEnvName)
	if activeProfile == "" {
		activeProfile = ""
	} else {
		activeProfile = fmt.Sprintf("-%s", activeProfile)
	}

	configFilePath := strings.ReplaceAll(configFilePathTemplate, "%PROFILE%", activeProfile)

	configstore.File(configFilePath)
	configstore.Env("")

	items, err := configstore.GetItemList()
	if err != nil {
		panic(err)
	}

	filter := configstore.Filter()
	items = filter.Squash().Apply(items)

	return &ConfigStoreUtils{items: items}
}

func (csu *ConfigStoreUtils) GetStrProp(name string) string {
	v, err := csu.items.GetItemValue(name)
	if err != nil {
		panic(err)
	}
	return v
}

func (csu *ConfigStoreUtils) GetIntProp(name string) int64 {
	v, err := csu.items.GetItemValueInt(name)
	if err != nil {
		panic(err)
	}
	return v
}

func (csu *ConfigStoreUtils) GetBoolProp(name string) bool {
	v, err := csu.items.GetItemValueBool(name)
	if err != nil {
		panic(err)
	}
	return v
}

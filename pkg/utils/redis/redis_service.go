package redis

import "time"

type RedisService interface {
	Get(key string, v interface{}) error

	Put(key string, v interface{}) error

	PutExpirable(key string, v interface{}, expiration time.Duration)

	Remove(key string)

	SetExpire(key string, expiration time.Duration)
}

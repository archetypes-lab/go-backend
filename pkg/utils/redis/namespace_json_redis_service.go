package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	goredis "github.com/go-redis/redis/v8"
)

type NamespaceJsonRedisService struct {
	redisClient *goredis.Client
	context     context.Context
}

func (service *NamespaceJsonRedisService) Get(key string, v interface{}) error {
	jsonRaw, errRedis := service.redisClient.Get(service.context, key).Result()
	if errRedis != nil {
		return errRedis
	}
	return json.Unmarshal([]byte(jsonRaw), v)
}

func (service *NamespaceJsonRedisService) Put(key string, v interface{}) error {
	return service.PutExpirable(key, v, 0)
}

func (service *NamespaceJsonRedisService) PutExpirable(key string, v interface{}, expiration time.Duration) error {
	marshalBinary, errJson := json.Marshal(v)
	if errJson != nil {
		return errJson
	}
	marshalString := fmt.Sprint(marshalBinary)
	return service.redisClient.Set(service.context, key, marshalString, expiration).Err()
}

func (service *NamespaceJsonRedisService) Remove(key string) error {
	return service.redisClient.Del(service.context, key).Err()
}

func (service *NamespaceJsonRedisService) SetExpire(key string, expiration time.Duration) error {
	return service.redisClient.Expire(service.context, key, expiration).Err()
}

package utils

type ConfigPropertiesUtils interface {
	GetStrProp(name string) string
	GetIntProp(name string) int64
	GetBoolProp(name string) bool
}

package db

import (
	"fmt"
	"strings"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"go.uber.org/zap"
)

type PostgresMigrationUtils struct {
	migrate *migrate.Migrate
	logger  *zap.Logger
}

func CreatePostgresMigrationUtils(user string, password string, host string, port int, database string, sslMode string, logger *zap.Logger) *PostgresMigrationUtils {
	databaseUrl := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", user, password, host, port, database, sslMode)
	m, err := migrate.New(
		"file://scripts/db/migrations",
		databaseUrl)
	if err != nil {
		panic(err)
	}
	return &PostgresMigrationUtils{migrate: m, logger: logger}
}

func (mc *PostgresMigrationUtils) PostConstruct() {
	mc.logger.Info("-> Migrating Database!")
	if err := mc.migrate.Up(); err != nil && !strings.HasPrefix(err.Error(), "no change") {
		panic(err)
	} else {
		mc.logger.Info("OK - Migration updated!")
	}
}
